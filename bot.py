# -*- coding: utf-8 -*-
'''
Created by Senchick Projects.
Users: stanislav
Date: 26/06/2018
Time: 23:34
'''
import time
import vk_api

login, password = '', '' # логин и пароль

vk = vk_api.VkApi(login = login, password = password)

try:
	vk.auth() # авторизация
except vk_api.AuthError as error_msg:
	print('auth error', ':', error_msg) # вывод ошибки авторизации, если таковая имела место быть

print('successful auth for', login) # вывод подтверждения успешной авторизации

values = {'out': 0,'count': 100,'time_offset': 60}

def send(chat_id, msg):
	vk.method('messages.send', {'chat_id':chat_id,'message':msg}) # документация: https://vk.com/dev/methods/messages.send

def reply(reply_id):
	replies = [ # варианты ответов, от нуля до бесконечности
	'да да, я', 
	'пизда', 
	'в пизде', 
	'в очко своему отчиму', 
	'никак, иди нахуй', 
	'всегда пожалуйста', 
	'https://bit.ly/1c9uOkF', 
	'твой батя пидорас', 
	'а твоя мать это твой второй батя']

	sent = 'was successfully sent' # текст подтверждения успешной отправки сообщения
	msg = replies[reply_id]

	send(item['chat_id'], msg) # отправка сообщения
	print(msg, sent) # вывод подтверждения успешной отправки сообщения
	time.sleep(3) # задержка / антиспам

while True:
	response = vk.method('messages.get', values)
	if response['items']:
		values['last_message_id'] = response['items'][0]['id']
	for item in response['items']:
		get = response['items'][0]['body'].lower() # приведение текста к нижнему регистру
		if get == 'ты просто космос, стас':
				reply(0)
		elif get == 'да':
				reply(1)
		elif get == 'где':
				reply(2)
		elif get == 'куда':
				reply(3)
		elif get == 'как':
				reply(4)
		elif get == 'спасибо':
				reply(5)
		elif get == 'дайте ссылку':
				reply(6)
		elif get == 'ты гей':
				reply(7)
				reply(8)